# TODO / TASKS MANAGER - EXERCICE 8

## Description

Voici une simple application qui a pour but de gérer la liste des tâches à faire. Elle permet d'ajouter, de supprimer et de marquer une tâche comme terminée.

Elle a été réalisée avec Java Spring Boot.

## API

Une fois lancée, accédez à l'API via l'URL suivante : `http://localhost:8080/swagger-ui.html`

## Tests

### Configuration du Pipeline CI/CD

Pour automatiser le processus de build, test et déploiement de l'application, nous avons mis en place un pipeline CI/CD avec GitLab CI. Voici les étapes nécessaires pour configurer ce pipeline.

### Fichier `.gitlab-ci.yml`

Le fichier `.gitlab-ci.yml` est le cœur de la configuration du pipeline CI/CD. Il définit les différentes étapes du pipeline, les variables d'environnement et les services nécessaires, tels que MongoDB pour les tests d'intégration.

#### Configuration du fichier `.gitlab-ci.yml`

```yaml
image: eclipse-temurin:22-jdk

stages:
  - build
  - test
  - package

variables:
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  PROJECT_DIR: "D:/Projets/todoapp"
  MONGO_HOST: "mongo"
  MONGO_PORT: "27017"

services:
  - name: mongo:latest
    alias: mongo

cache:
  paths:
    - .m2/repository
    - target

before_script:
  - echo "Installation de Maven"
  - apt-get update
  - apt-get install -y maven
  - mvn -version

build:
  stage: build
  script:
    - mvn $MAVEN_CLI_OPTS clean compile
  only:
    - main

test:
  stage: test
  script:
    - mvn $MAVEN_CLI_OPTS -Dspring.profiles.active=ci test
  only:
    - main

package:
  stage: package
  script:
    - mvn $MAVEN_CLI_OPTS -Dspring.profiles.active=ci package
  only:
    - main
  artifacts:
    paths:
      - target/*.jar
```

### Profils de configuration Spring Boot

Pour permettre une configuration différente en local et dans le pipeline CI/CD, nous avons utilisé des profils Spring Boot.

#### application.properties (Configuration par défaut pour l'exécution locale)

```properties
# Informations application
spring.application.name=todoapp

# Informations de connexion DB pour l'exécution locale
spring.data.mongodb.uri=mongodb://localhost:27017/todoapp

logging.level.org.springdoc=DEBUG
```

#### application-ci.properties (Configuration pour CI/CD)

```properties
# Informations application
spring.application.name=todoapp

# Informations de connexion DB pour CI/CD
spring.data.mongodb.uri=mongodb://${MONGO_HOST:localhost}:${MONGO_PORT:27017}/todoapp

logging.level.org.springdoc=DEBUG
```

### Configuration des Webhooks

Pour configurer des webhooks dans GitLab, suivez les étapes suivantes :

1. Accédez à votre projet dans GitLab.
2. Allez dans les "Settings" du projet.
3. Sélectionnez "Webhooks" dans le menu de gauche.
4. Cliquez sur "Add webhook".
5. Entrez l'URL de votre service de webhook.
6. Sélectionnez les événements pour lesquels vous souhaitez recevoir des notifications (par exemple, push events, merge requests).
7. Cliquez sur "Add webhook".

### Configuration des Agents de Build

Les agents de build sont configurés dans GitLab Runner. Pour installer et enregistrer un GitLab Runner, suivez ces étapes :

1. Téléchargez et installez GitLab Runner en suivant les instructions officielles [ici](https://docs.gitlab.com/runner/install/).
2. Enregistrez le runner avec votre projet GitLab :
   ```sh
   gitlab-runner register
   ```
   Suivez les instructions pour fournir l'URL de votre instance GitLab et le token d'enregistrement du projet.

### Notifications de Résultats

Pour configurer les notifications de résultats, vous pouvez utiliser des services intégrés à GitLab comme les notifications par e-mail, Slack ou d'autres intégrations de chat.

1. Accédez aux "Settings" de votre projet dans GitLab.
2. Sélectionnez "Integrations" dans le menu de gauche.
3. Choisissez le service de notification que vous souhaitez configurer (par exemple, Slack notifications).
4. Suivez les instructions pour connecter GitLab à votre service de notification et configurer les événements pour lesquels vous souhaitez recevoir des notifications.

### Exécution du pipeline en local

Pour exécuter l'application en local avec la configuration par défaut :

```sh
mvn spring-boot:run
```

Pour exécuter l'application avec le profil CI/CD :

```sh
mvn spring-boot:run -Dspring-boot.run.profiles=ci
```

Avec cette configuration, votre pipeline CI/CD est bien défini et les différentes étapes sont automatisées pour assurer un processus de build, test et déploiement efficace et fiable.
