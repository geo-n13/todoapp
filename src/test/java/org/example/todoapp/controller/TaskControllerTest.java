package org.example.todoapp.controller;

import org.example.todoapp.model.Task;
import org.example.todoapp.repository.TaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskControllerTest {

    @Mock
    private TaskRepository taskRepository;

    @InjectMocks
    private TaskController taskController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(taskController).build();
    }

    @Test
    public void testGetAllTasks() throws Exception {
        Task task1 = new Task();
        task1.setId("1");
        task1.setName("Task 1");
        task1.setCompleted(false);

        Task task2 = new Task();
        task2.setId("2");
        task2.setName("Task 2");
        task2.setCompleted(true);

        when(taskRepository.findAll()).thenReturn(Arrays.asList(task1, task2));

        mockMvc.perform(get("/tasks"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].name").value("Task 1"))
                .andExpect(jsonPath("$[0].completed").value(false))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].name").value("Task 2"))
                .andExpect(jsonPath("$[1].completed").value(true));

        verify(taskRepository, times(1)).findAll();
    }

    @Test
    public void testGetTaskById() throws Exception {
        Task task = new Task();
        task.setId("1");
        task.setName("Task 1");
        task.setCompleted(false);

        when(taskRepository.findById(anyString())).thenReturn(Optional.of(task));

        mockMvc.perform(get("/tasks/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Task 1"))
                .andExpect(jsonPath("$.completed").value(false));

        verify(taskRepository, times(1)).findById("1");
    }

    @Test
    public void testCreateTask() throws Exception {
        Task task = new Task();
        task.setId("1");
        task.setName("New Task");
        task.setCompleted(false);

        when(taskRepository.save(any(Task.class))).thenReturn(task);

        mockMvc.perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"New Task\", \"completed\": false}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("New Task"))
                .andExpect(jsonPath("$.completed").value(false));

        verify(taskRepository, times(1)).save(any(Task.class));
    }

    @Test
    public void testUpdateTask() throws Exception {
        Task task = new Task();
        task.setId("1");
        task.setName("Updated Task");
        task.setCompleted(true);

        when(taskRepository.save(any(Task.class))).thenReturn(task);

        mockMvc.perform(put("/tasks/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Updated Task\", \"completed\": true}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Updated Task"))
                .andExpect(jsonPath("$.completed").value(true));

        verify(taskRepository, times(1)).save(any(Task.class));
    }

    @Test
    public void testDeleteTask() throws Exception {
        // Simulez que la tâche existe
        when(taskRepository.existsById(anyString())).thenReturn(true);
        doNothing().when(taskRepository).deleteById(anyString());

        mockMvc.perform(delete("/tasks/1"))
                .andExpect(status().isOk());

        verify(taskRepository, times(1)).existsById("1");
        verify(taskRepository, times(1)).deleteById("1");
    }

}
