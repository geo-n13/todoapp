package org.example.todoapp.controller;

import org.example.todoapp.model.Task;
import org.example.todoapp.repository.TaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TaskControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskRepository taskRepository;

    @BeforeEach
    public void setUp() {
        taskRepository.deleteAll();
    }

    @Test
    public void testCreateAndGetTask() throws Exception {
        // Test creating a new task
        mockMvc.perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Integration Test Task\", \"completed\": false}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Integration Test Task"))
                .andExpect(jsonPath("$.completed").value(false));

        // Test retrieving the created task
        mockMvc.perform(get("/tasks"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Integration Test Task"))
                .andExpect(jsonPath("$[0].completed").value(false));
    }

    @Test
    public void testUpdateTask() throws Exception {
        // Create a task to update
        Task task = new Task();
        task.setName("Old Task");
        task.setCompleted(false);
        task = taskRepository.save(task);

        // Test updating the task
        mockMvc.perform(put("/tasks/" + task.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"Updated Task\", \"completed\": true}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Updated Task"))
                .andExpect(jsonPath("$.completed").value(true));

        // Verify the update
        mockMvc.perform(get("/tasks/" + task.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Updated Task"))
                .andExpect(jsonPath("$.completed").value(true));
    }

    @Test
    public void testDeleteTask() throws Exception {
        // Create a task to delete
        Task task = new Task();
        task.setName("Task to be deleted");
        task.setCompleted(false);
        task = taskRepository.save(task);

        // Test deleting the task
        mockMvc.perform(delete("/tasks/" + task.getId()))
                .andExpect(status().isOk());

        // Verify the task has been deleted
        mockMvc.perform(get("/tasks/" + task.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }
}
