package org.example.todoapp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.example.todoapp.model.Task;
import org.example.todoapp.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Operation(summary = "View a list of available tasks")
    @GetMapping
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    @Operation(summary = "Get a task by Id")
    @GetMapping("/{id}")
    public Task getTaskById(
            @Parameter(description = "ID of the task to be obtained")
            @PathVariable String id) {
        Optional<Task> task = taskRepository.findById(id);
        return task.orElse(null);
    }

    @Operation(summary = "Create a new task")
    @PostMapping
    public Task createTask(
            @Parameter(description = "Task object to be created")
            @RequestBody Task task) {
        return taskRepository.save(task);
    }

    @Operation(summary = "Update an existing task")
    @PutMapping("/{id}")
    public Task updateTask(
            @Parameter(description = "ID of the task to be updated")
            @PathVariable String id,
            @Parameter(description = "Updated task object")
            @RequestBody Task task) {
        task.setId(id);
        return taskRepository.save(task);
    }

    @Operation(summary = "Delete a task by Id")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@Parameter(description = "ID of the task to be deleted") @PathVariable String id) {
        if (taskRepository.existsById(id)) {
            taskRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
