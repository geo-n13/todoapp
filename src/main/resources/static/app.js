const apiUrl = 'http://localhost:8080/tasks';

document.addEventListener('DOMContentLoaded', fetchTasks);

function fetchTasks() {
    fetch(apiUrl)
        .then(response => response.json())
        .then(tasks => {
            console.log('Fetched tasks:', tasks); // Log the fetched tasks
            const tasksDiv = document.getElementById('tasks');
            tasksDiv.innerHTML = '';
            tasks.forEach(task => {
                const taskElement = document.createElement('div');
                taskElement.className = 'task';
                taskElement.innerHTML = `
                    <h3>${task.name ? task.name : "No name"}</h3>
                    <p>Completed: ${task.completed ? "Yes" : "No"}</p>
                    <button onclick="deleteTask('${task.id}')">Delete</button>
                    <button onclick="editTask('${task.id}', '${task.name}', ${task.completed})">Edit</button>
                `;
                tasksDiv.appendChild(taskElement);
            });
        })
        .catch(error => console.error('Error fetching tasks:', error));
}

function saveTask() {
    const id = document.getElementById('task-id').value;
    const name = document.getElementById('task-name').value;
    const completed = document.getElementById('task-completed').checked;

    const task = { name, completed };
    console.log('Saving task:', task); // Log the task being saved

    if (id) {
        // Update existing task
        fetch(`${apiUrl}/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(task)
        })
            .then(response => response.json())
            .then(() => {
                fetchTasks();
                clearForm();
            })
            .catch(error => console.error('Error updating task:', error));
    } else {
        // Create new task
        fetch(apiUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(task)
        })
            .then(response => response.json())
            .then(() => {
                fetchTasks();
                clearForm();
            })
            .catch(error => console.error('Error creating task:', error));
    }
}

function deleteTask(id) {
    fetch(`${apiUrl}/${id}`, {
        method: 'DELETE'
    })
        .then(() => {
            fetchTasks();
        })
        .catch(error => console.error('Error deleting task:', error));
}

function editTask(id, name, completed) {
    document.getElementById('task-id').value = id;
    document.getElementById('task-name').value = name !== "undefined" ? name : "";
    document.getElementById('task-completed').checked = completed;
}

function clearForm() {
    document.getElementById('task-id').value = '';
    document.getElementById('task-name').value = '';
    document.getElementById('task-completed').checked = false;
}
